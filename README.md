
These are brief notes for [Gautam Iyer's](http://www.math.cmu.edu/~gautam) undergraduate discrete time finance course.
I taught remotely in [Fall 2021](http://www.math.cmu.edu/~gautam/sj/teaching/2021-22/370-dtime-finance/) and typed minimal slides containing only theorem statements and definitions. A PDF of these, and an annotated version with hand-written proofs can be found on this website.

If you use these notes and find typos, or want to change them in any way, please consider contributing your changes back here.
